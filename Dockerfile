FROM golang:1.19-alpine

WORKDIR /myapp

COPY ./ ./
RUN go mod download

RUN go build -o app main.go

EXPOSE 5009

CMD ["./app"]