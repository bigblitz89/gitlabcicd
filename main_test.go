package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSayHello(t *testing.T) {
	tests := []struct {
		name       string
		givePhraze string
		wantResult string
	}{
		{
			name:       "all_correct",
			givePhraze: "Gitlab CI/CD",
			wantResult: "Hi Gitlab CI/CD",
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			assert.Equal(t, test.wantResult, sayHello(test.givePhraze))
		})
	}
}
